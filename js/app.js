const inputDOM = document.getElementById('input')
const buttonDOM = document.getElementById('add')
const listsDOM = document.getElementById('lists')
const btnBgColDOM = document.getElementById('bg-col')
const localStorage = window.localStorage

const li = function (index, input, finished = false) {
    const status = finished ? `<span class="badge badge-danger">finished</span>` : ''
    const isFinish = finished ? 'finished' : ''
    const toolTipClass = !finished ? 'tooltip-question' : ''
    const toolTipText = !finished ? `title="Did you finished doing it?"` : ''
    return `<li class="list-item align-items-center">
        <span ${toolTipText} class="list-item-value ${isFinish} ${toolTipClass}" onclick='finishedItem(${index})' >
            ${status} ${input}
        </span>
        <span onclick='deleteItem(${index})'>
            <img class="list-item-info" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGuSURBVGhD7dm7SsRAFMbx9QIWiihWWvgKgmhjI/oI1hZWYuOtUbG0EBQfwjcQQWzEQtFWQSwsFGwEfQgv/1MMDMMZjSSzZ4vzwa/JTjb5lmR2M9vyeDwmGcRwRjc6Pn24wvcvPjCFtqcHC1iu4ADayacuoe2fWsIoGskRtJNpl1fIh1k759AO0C6fGELtTOIGLwYesQqPp9MzBu0aLm0FjWYc2oxS2g4ajRepyYvk4kVq8iK5VCkiv1I3cBxt09xjDWfRthyTIjMI2YY25hr9kPRCymvjApMi04iTlpESAwiRR1759o7HpEyKPGAEcUKZtISkygOb2c0u139aZhFpiUNo+6fMigitTJyqJYRpEXEHrcx/SgjzIvHsFGcL2vgc0yK5EiH/KWNWRCuxjvQyq1rGpMgt0hLhntAmgF2k75EyKTKBOOmNnZbpwhPiMSmTIvMIyc1O8Wwm68Rv0MYFJkVkcXoff61MPmMPcilqr8fMbvameZFcvEhNjReRP1u0A5W2icbzDu1gJc2i8chzxRe0A5ZwimKZgywuXBR0AvlzR57pPR6Px9PJabV+AETdelUqvnnHAAAAAElFTkSuQmCC">
        </span>
    </li>`
}

const render = function () {
    const listItems = JSON.parse(localStorage.getItem('lists'))

    listsDOM.innerHTML = listItems.map(function (input, index) {
        return li(index, input.value, input.finished)
    }).join('')

    tippy(document.querySelectorAll('.tooltip-question'), {
        delay: 0,
        arrow: true,
        arrowType: 'large',
        size: 'large',
        duration: [350, 300],
        animation: 'perspective'
    })

    tippy(document.querySelectorAll('.jscolor'), {
        delay: 0,
        arrow: true,
        arrowType: 'large',
        duration: [350, 300],
        animation: 'shift-toward'
    })
}

const getItems = function () {
    return JSON.parse(localStorage.getItem('lists'))
}

const addItem = function (inputValue) {
    if (localStorage.getItem('lists') == null) {
        localStorage.setItem('lists', JSON.stringify([]))
    }
    if (inputValue != '') {
        const storedData = getItems()
        const dataItem = {
            value: inputValue,
            finished: false
        }
        storedData.push(dataItem)
        localStorage.setItem('lists', JSON.stringify(storedData))

        inputDOM.value = ''
        inputDOM.focus()

        render()
    }
}

const finishedItem = function (index) {
    const data = getItems()
    data[index].finished = !data[index].finished
    localStorage.setItem('lists', JSON.stringify(data))

    render()
}

const deleteItem = function (index) {
    const data = getItems()
    delete data[index]
    filteredData = data.filter(function (x) { return data != null })
    localStorage.setItem('lists', JSON.stringify(filteredData))

    render()
}

buttonDOM.addEventListener('click', function () {
    addItem(inputDOM.value)
})

inputDOM.addEventListener('keypress', function (e) {
    if (e.key == 'Enter') {
        addItem(this.value)
    }
})

const changeBgCol = function () {
    document.body.style.backgroundColor = `#${btnBgColDOM.value}`
}

render()